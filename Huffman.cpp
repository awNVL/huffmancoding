#include "Huffman.h"

Huffman::Huffman():text(""),codeStr(""),root(0){
}

void Huffman::Scan(){
	cout<<"Please press any text ending with Enter:"<<endl;
	getline(cin,text);
	GetWeight();
}

void Huffman::HuffmanCoding(){
	for(int i=0;i<stored.size();i++)		//建立节点的优先队列 
		nodeHeap.push(stored[i]);
	MergeTree();
	Encoding();
}

void Huffman::Output(){
	cout<<"After huffman coding,the text is encoded as follows:"<<endl;
	for(int i=0;i<text.length();i++)
		CharCodeOutput(text[i]);
	cout<<endl;
}

void Huffman::Detail(){
	for(int i=0;i<charCode.size();i++)
		cout<<charCode[i].ch<<":"<<charCode[i].code<<endl;
}

void Huffman::ProcessChar(char c){
	for(int i=0;i<stored.size();i++){
		if(c==(stored[i]->ch)){
			++(stored[i]->weight);		//若已经存储过了，则增加权重 
			return;
		}
	}
	Node* temp=new Node(c,1);		//第一次遇到该字符，创建一个节点并存储到stored中，权重为1 
	stored.push_back(temp);
}

void Huffman::GetWeight(){
	for(int i=0;i<text.length();i++)
		ProcessChar(text[i]);
}

void Huffman::MergeTree(){
	while(!nodeHeap.empty()){
		Node *cLeft=nodeHeap.top();
		nodeHeap.pop();
		Node *cRight=nodeHeap.top();
		nodeHeap.pop();
		Node *mergeNode=new Node(0,cLeft->weight+cRight->weight);
		mergeNode->left=cLeft;
		mergeNode->right=cRight;
		nodeHeap.push(mergeNode);
		if(nodeHeap.size()==1)		//得到了最终的编码树在优先队列的顶部
			break; 
	}
	root=nodeHeap.top();
	nodeHeap.pop();
}

void Huffman::Encoding(){
	Encoding(root);
}

void Huffman::Encoding(Node *t){
	if(t==NULL)
		return;
	if((t->left==NULL)&&(t->right==NULL)){
		EncodeNode temp(t->ch,codeStr);
		charCode.push_back(temp);
		return;
	}
	if(t->left!=NULL){
		string oldStr=codeStr;		//保存当前节点的编码字符串 
		codeStr+="0";
		Encoding(t->left);
		codeStr=oldStr;						//重新设置编码字符串为当前节点的编码字符串 
	}
	if(t->right!=NULL){
		string oldStr=codeStr;		//保存当前节点的编码字符串 
		codeStr+="1";
		Encoding(t->right);
		codeStr=oldStr;						//重新设置编码字符串为当前节点的编码字符串  
	}
}

void Huffman::CharCodeOutput(char c){
	for(int i=0;i<charCode.size();i++){
		if(c==charCode[i].ch){
			cout<<charCode[i].code;
			return;
		}
	}
}
