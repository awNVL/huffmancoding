#include "Huffman.h"

int main(){
	Huffman encoder;
	encoder.Scan();
	encoder.HuffmanCoding();
	encoder.Output();
	encoder.Detail();
	
	return 0;
}
