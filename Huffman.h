#ifndef HUFFMAN_H
#define HUFFMAN_H
#include <iostream>
#include <queue>
#include <vector>
#include <string>
using namespace std;

class Huffman{
	public:
		Huffman();				//构造器
		void Scan();			//读取要编码的字符串或文本
		void HuffmanCoding();		//为字符串或文本编码 
		void Output();		//输出编码后的代码 
		void Detail();
		
	protected:
		class Node{				//字符节点定义 
			public:
				Node(char cvalue,int wvalue):ch(cvalue),weight(wvalue),left(0),right(0){}
				
				char ch;			//字符
				int weight;		//权重，即字符出现的频率 
				
				Node *left;
				Node *right;
		};
		
		class EncodeNode{		//字符编码节点定义 
			public:
				EncodeNode(char cvalue,string codeStr):ch(cvalue),code(codeStr){}
				
				char ch;				//字符
				string code;		//编码字符串 
		};
		
		class LightWeightTop{		//比较类，放在priority_queue的模板初始化的第三个参数中，以便在priority_queue中排序
			public:
				bool operator ()(const Node* node1,const Node* node2){
					return (node1->weight)>(node2->weight);		//这里是权重小的节点放在顶部 
				}
		};
		
		void ProcessChar(char c);		//在stored中查找某个字符是否已经被存储了,若是，则递增其节点的权重，若否，则存储起来 
		void GetWeight();	//统计不同字符出现的频率 
		void MergeTree();	//合并两个最小权重的节点 
		void Encoding();	//进行编码，调用递归例程Encoding	
		
		void Encoding(Node *t);					//递归例程，在霍夫曼编码树中查找无子节点的节点，然后创建编码节点并存储在charNode中 
		void CharCodeOutput(char c);					//字符编码输出 
		
		string text;			//存储输入的字符串或文本 
		string codeStr;		//存储编码字符串 
		vector<Node*> stored;		//存储字符节点
		vector<EncodeNode> charCode;		//存储字符编码节点 
		priority_queue<Node*,vector<Node*>,LightWeightTop> nodeHeap;		//节点的优先队列，权重小的节点在顶部
		Node *root;				//最终得到的霍夫曼编码树
};
#endif
